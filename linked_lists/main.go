package main

import "fmt"

type Node interface {
	SetValue(v int)
	GetValue() int
}

type SLLNode struct {
	next  *SLLNode
	value int
}

func (sNode *SLLNode) SetValue(v int) {
	sNode.value = v
}
func (sNode *SLLNode) GetValue() int {
	return sNode.value
}
func NewSLLNode() *SLLNode {
	return new(SLLNode)
}

type PowerNode struct {
	next  *SLLNode
	value int
}
func (pNode *PowerNode) SetValue(v int) {
	pNode.value = v * 10
}
func (pNode *PowerNode) GetValue() int{
	return pNode.value
}
func NewPowerNode()*PowerNode{
	return new(PowerNode)
}

func main() {
	// node of type Node (from the Node interface)
	var node Node
	// assigning SLLNode to node.
	node = NewSLLNode()
	node.SetValue(10)
	fmt.Println(node.GetValue())
	// assigning PowerNode to node
	node = NewPowerNode()
	node.SetValue(10)
	fmt.Println(node.GetValue())

	// to verify if it is of a type of PowerNode
	if n,ok := node.(*PowerNode); ok{
		fmt.Println("This is a power node of value: ",n.value)
	}

}

