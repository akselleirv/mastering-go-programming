package main

import (
	"fmt"
	"time"
)

// Why use timer instead of Ticker?
// * with a timer you control when the time duration will start
// * a ticker will not wait for a code to finish executing

// This is an example of how to execute code in a timely fashion
func main() {
	nc := make(chan int)
	stopc := make(chan bool)

	go SlowCounter(1, nc, stopc)
	time.Sleep(5 * time.Second)
	//Changing sleep to 2
	nc <- 2
	time.Sleep(6 * time.Second)
	stopc <- true //stopping loop
	time.Sleep(1 * time.Second)
}
func SlowCounter(n int, nc chan int, stopc chan bool) {
	i := 0
	// Create a duration of n seconds
	d := time.Duration(n) * time.Second

Loop: //Using label "Loop" in order to break out of for-loop
	for {
		select {
		// Use time.after channel to wait for a time period
		case <-time.After(d):
			i++
			fmt.Println(i)
		case n = <-nc:
			fmt.Println("Timer duration changed to", n)
			d = time.Duration(n) * time.Second
		case <-stopc:
			fmt.Println("Timer stopped")
			break Loop
		}
	}
	fmt.Println("Exiting slow counter")
}
