package main

import (
	"fmt"
	"reflect"
)

type Printer interface {
	Print(s string)
}
type pStruct struct {
	s string
}

func (p *pStruct) Print(s string){
	p.s = s
	fmt.Println(s)
}
func main(){
	p:= new(pStruct)
	inspectType(p)
}

func inspectType(obj interface{}){
	// reflected value object of obj
	v := reflect.ValueOf(obj)
	// getting type of value
	t := v.Type()
	// checking if obj has implemented interface
	// typecast interface type. will type cast a nil value.
	// getting the ref object with Elem() which will represent type Printer
	// we do not care about the value
	myInterface := reflect.TypeOf((*Printer)(nil)).Elem()

	// checks if type implements the interface
	fmt.Println("Obj implements Printer?",t.Implements(myInterface))

	if t.Implements(myInterface){
		// gets a reflection value object which represent the Print method
		printFunc := v.MethodByName("Print")
		// creating the arguments in order to call the function
		// create a string and convert it to a value object
		args := []reflect.Value{reflect.ValueOf("Printing Hello from a reflection object")}
		// invoke the method by pasing arguments to it
		printFunc.Call(args)
	}
}