package main

import (
	"io"
	"log"
	"os"
)

func main(){
	//open a file for read only
	f1,err := os.Open("test1.txt")
	PrintFatalError(err)
	defer f1.Close()

	//Create a new file
	f2, err := os.Create("test2.txt")
	PrintFatalError(err)
	defer f2.Close()

	//Open for file for read and write
	f3, err := os.OpenFile("test3.txt",os.O_APPEND|os.O_RDWR,0666)
	PrintFatalError(err)
	defer f3.Close()

	//Rename a file
	err = os.Rename("test1.txt","test1New.txt")
	PrintFatalError(err)

	//move a file
	err = os.Rename("./test1.txt","./testfolder/test1.txt")
	PrintFatalError(err)

	//copy a file
	CopyFile("test3.txt","./testfolder/test3.txt")

	//delete a file
	err = os.Remove("test2.txt")
	PrintFatalError(err)

}
func CopyFile(fname1,fname2 string){
	fold, err := os.Open(fname1)
	PrintFatalError(err)
	defer fold.Close()

	fNew, err := os.Create(fname2)
	PrintFatalError(err)
	defer fNew.Close()

	_,err = io.Copy(fNew,fold)
	PrintFatalError(err)

	//flush file contents to disk
	err = fNew.Sync()
	PrintFatalError(err)
}

func PrintFatalError(err error){
	if err != nil{
		log.Fatal("Error happened while processing file",err)
	}
}