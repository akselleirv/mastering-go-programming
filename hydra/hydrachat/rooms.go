package hydrachat

import (
	"fmt"
	"io"
	"sync"
)

type room struct {
	name    string
	Msgch   chan string
	clients map[chan<- string]struct{} //set data type. Unique clients
	Quit    chan struct{}
	*sync.RWMutex
}

func CreateRoom(name string) *room {
	r := &room{
		name:    name,
		Msgch:   make(chan string),
		RWMutex: new(sync.RWMutex),
		clients: make(map[chan<- string]struct{}),
		Quit:    make(chan struct{}),
	}
	r.Run()
	return r
}

// Starting the chat room
func (r *room) Run() {
	//Logger.Println("Starting chat room", r.name)
	go func() { // Listening to the msg-channel. If received it will be broadcasted.
		for msg := range r.Msgch {
			r.broadcastMsg(msg)
		}
	}()
}

// fan-out the message
func (r *room) broadcastMsg(msg string) {
	r.RLock()
	defer r.RUnlock()
	fmt.Println("Received message: ", msg)
	// Only getting the keys (clients). The channels the clients are listening to
	for wc, _ := range r.clients {
		// each client creates a goroutine.
		// copy the client as a parameter. If not the value might change during the loop
		// if the message is not read immediately, the channel would be blocked
		// therefore it is crucial that it is placed on a different goroutine
		go func(wc chan<- string) {
			// pass the message to the channel client is connected to
			wc <- msg
		}(wc)
	}
}

func (r *room) AddClient(c io.ReadWriteCloser) {
	r.Lock()
	// Create a channel generator
	wc, done := StartClient(r.Msgch, c, r.Quit)
	r.clients[wc] = struct{}{} // value is empty. Only care about key.
	r.Unlock()

	// remove client when done is signalled
	go func() {
		<-done // a signal waiting for the client disconnecting
		r.RemoveClient(wc)
	}()
}
//Remove the client from room
// A client is represented as the parameter "wc chan<- string"
func (r *room) RemoveClient(wc chan<- string) {
	//logger.Println("Removing client")
	r.Lock()
	//closing the client channel
	close(wc)
	//delete the client from the client map
	delete(r.clients, wc)
	r.Unlock()
	select {
	case <-r.Quit:
		// if chat room is empty => remove the room
		if len(r.clients) == 0{
			//closing the room and the goroutine in Run() will also exit.
			close(r.Msgch)
		}
	default:
		// the default is empty on purpose
		// if it default wasn't here. the "<-r.Quit" would block the select-statement
		}
}
