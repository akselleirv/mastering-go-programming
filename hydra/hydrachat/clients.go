package hydrachat

import (
	"bufio"
	"io"
)

type client struct {
	*bufio.Reader
	*bufio.Writer
	wc chan string
}

func StartClient(msgCh chan<- string, cn io.ReadWriteCloser, quit chan struct{}) (chan<- string, chan struct{}) {
	c := new(client)
	c.Reader = bufio.NewReader(cn)
	c.Writer = bufio.NewWriter(cn)
	c.wc = make(chan string)
	done := make(chan struct{})

	//setup the reader
	go func() {
		scanner := bufio.NewScanner(c.Reader)
		// this for-loop will stay alive as long the connection is up
		for scanner.Scan() {
			logger.Println(scanner.Text())
			msgCh <- scanner.Text()
		}
		// when the for-loop exits the done channel will be called
		done <- struct{}{}
	}()

	//setup the writer
	c.writeMonitor()

	go func() {
		select {
		case <-quit:
			// if the room connection is quiting the client connection will close
			cn.Close()
		case <-done:
		}
	}()

	// returning client write channel and done channel
	return c.wc, done
}

// sending the message to client when received on client channel
func (c *client) writeMonitor() {
	go func() {
		// when a client write channel is ready and sending
		// it will be written to the writer
		for s := range c.wc {
			logger.Println("Sending",s)
			c.WriteString(s + "\n")
			c.Flush()
		}
	}()
}
