package hydrachat

import (
	"fmt"
	"gitlab.com/akselsle/mastering-go-programming/hydra/hlogger"
	"net"
	"os"
	"os/signal"
	"syscall"
)

var logger = hlogger.GetInstance()

//start hydra chat
func Run(connection string) error {
	l, err := net.Listen("tcp", connection)
	if err != nil {
		logger.Println("Error connecting to chat client", err)
		return err
	}
	r := CreateRoom("HydraChat")
	go func() {
		// Handle SIGINT and SIGTERM
		ch := make(chan os.Signal)
		signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
		<-ch // listening on os signal. Block until it receives a signal

		// clean up the program
		l.Close()
		fmt.Println("Closing the tcp connection")
		close(r.Quit)
		/*if r.ClCount() > 0 {
			<-r.Msgch
		}*/
		os.Exit(0)
	}()

	for {
		conn, err := l.Accept() // will block until conn from new client
		if err != nil {
			logger.Println("Error accepting connection from chat client", err)
			break
		}
		go handleConnection(r, conn)
	}

	return err
}
func handleConnection(r *room, c net.Conn){
	logger.Println("Received request from client",c.RemoteAddr())
	r.AddClient(c)
}
