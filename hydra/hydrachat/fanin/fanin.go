package fanin

// take multiple channels as arguments and output on channel
// ... variadic function notation: a collection of arguments
func fanin(chs ...<-chan int) <-chan int {
	out := make(chan int)
	for _, c := range chs{
		// for all values coming from these channels
		// to show up on the output channel
		go registerChannel(c, out)
	}
	return out
}

func registerChannel(c <-chan int, out chan<- int) {
	// a blocking call
	for n := range c {
		out <- n
	}
}
