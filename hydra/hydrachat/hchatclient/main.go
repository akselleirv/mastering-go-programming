package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	name := fmt.Sprintf("Anonymous%d", rand.Intn(400))
	fmt.Println("Starting hydraChatClient...")
	fmt.Println("What's your name?")
	fmt.Scanln(&name)

	fmt.Printf("Hello %s, connecting to the hydra chat system... \n", name)
	// connecting to the chat server
	conn, err := net.Dial("tcp", "127.0.0.1:2300")
	if err != nil {
		log.Fatal("Could not connect to hydra chat system", err)
	}
	fmt.Println("Connected to Hydra Chat System")
	name += ":"
	defer conn.Close()
	go func() {
		//listening on incoming messages from the tcp connection
		scanner := bufio.NewScanner(conn)
		//listening for messages
		for scanner.Scan() {
			// print out message when received
			fmt.Println(scanner.Text())
		}
	}()

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() && err == nil {
		msg := scanner.Text()
		_, err = fmt.Fprintf(conn, name+msg+"\n")
	}
}
