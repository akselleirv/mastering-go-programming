package hydraproto

import (
	"errors"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	"log"
	"net"
)

type ProtoHandler struct{}

func NewProtoHandler() *ProtoHandler {
	return new(ProtoHandler)
}

func (pSender *ProtoHandler) EncodeAndSend(obj interface{}, destination string) error {
	v, ok := obj.(*Ship)
	if !ok {
		return errors.New("Proto: Unknown message type")
	}
	data, err := proto.Marshal(v)
	if err != nil {
		return err
	}
	return sendMessage(data, destination)
}

func (pSender *ProtoHandler) DecodeProto(buffer []byte) (*Ship, error) {
	pb := new(Ship)
	return pb, proto.Unmarshal(buffer, pb)
}

// a channel generator pattern function
func (pSender *ProtoHandler) ListenAndDecode(listenAddress string) (chan interface{}, error) {
	// interface chan can host any data type
	outChan := make(chan interface{})
	l, err := net.Listen("tcp", listenAddress)
	if err != nil {
		return outChan, err
	}
	log.Println("Listening to ", listenAddress)
	// creating function literal for handling tcp connections
	go func() {
		defer l.Close()
		for {
			c, err := l.Accept()
			if err != nil {
				break
			}
			log.Println("Accepted Conection from ", c.RemoteAddr())
			//handling a new connection on another goroutine
			//so the function won't be blocked
			//sending the conn as arguments so it doesn't change
			go func(c net.Conn) {
				defer c.Close()
				for {
					//process the data from the tcp-conn
					buffer, err := ioutil.ReadAll(c)
					if err != nil {
						break
					}
					if len(buffer) == 0 {
						continue
					}
					obj, err := pSender.DecodeProto(buffer)
					if err != nil {
						continue
					}
					// need a select-statement, because:
					// we do not need to wait if nobody is listening on
					// the channel.

					// it is possible to have a second case with timeout
					// case <-time.After(1 * time.Second):
					// this will trigger a channel once this time frame has elapsed
					// if it uses more than one second it will ignore the outChan and
					// go to the next data read from the tcp-conn
					select {
					case outChan <- obj:
					default:
					}
				}
			}(c)
		}
	}()
	//returning the channel we are feeding the data to
	return outChan, nil
}

func sendMessage(buffer []byte, dest string) error {
	conn, err := net.Dial("tcp", dest)
	if err != nil {
		return err
	}
	defer conn.Close()
	log.Printf("Sending %d bytes to %s \n", len(buffer), dest)
	_, err = conn.Write(buffer)
	return err
}
