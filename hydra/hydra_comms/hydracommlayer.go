package hydra_comms

import "gitlab.com/akselsle/mastering-go-programming/hydra/hydra_comms/hydraproto"

const (
	Protobuf uint8 = iota //enum
)

type HydraConnection interface {
	EncodeAndSend(obj interface{},destination string)error
	ListenAndDecode(listenaddress string) (chan interface{},error) //Channel generator
}
//Factory design pattern
func NewConnection(connType uint8) HydraConnection{
	switch connType {
	case Protobuf:
		return hydraproto.NewProtoHandler()
	}
	return nil
}
