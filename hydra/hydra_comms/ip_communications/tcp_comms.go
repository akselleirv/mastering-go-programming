package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

func main() {
	op := flag.String("type", "", "Server(s) or Client(c) ?")
	address := flag.String("addr", ":8000", "address=? host:port")
	flag.Parse()

	switch strings.ToUpper(*op) {
	case "S":
		runServer(*address)
	case "C":
		runClient(*address)
	}
}
func runClient(address string) error {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return err
	}
	defer conn.Close()

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("What message would you like to send?")
	for scanner.Scan() {
		fmt.Println("Writing ", scanner.Text())
		// \r identifier for when the message has ended
		conn.Write(append(scanner.Bytes(), '\r'))

		fmt.Println("What message would you like to send?")
		//Reading incoming message on the tcp conn
		buffer := make([]byte, 1024)
		_, err := conn.Read(buffer)
		if err != nil && err != io.EOF {
			log.Fatal(err)
		} else if err == io.EOF { //connection was closed
			log.Println("Connection is closed")
			return nil
		}
		//printing the message from the server
		fmt.Println(string(buffer))
	}
	//indicates if there were any errors during the scanner process
	return scanner.Err()
}

func runServer(address string) error {
	l, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}
	log.Println("Listening")
	defer l.Close()
	for {
		//Accept() will block loop until new client is connecting
		c, err := l.Accept()
		if err != nil {
			return err
		}
		// send the conn to another goroutine so the server doesn't hang
		go handleConnection(c)
	}
}
func handleConnection(c net.Conn) {
	defer c.Close()
	reader := bufio.NewReader(c)
	writer := bufio.NewWriter(c)

	for {
		// another option:
		// 1) create a buffer and 2) call c.Read()
		//buffer := make([]byte, 1024)
		//_, err := c.Read(buffer)
		// however it easier to use bufio and it makes for a better code design

		// setting a timeout for 5 seconds
		c.SetDeadline(time.Now().Add(5 * time.Second))
		// \r end of string
		line, err := reader.ReadString('\r')
		if err != nil && err != io.EOF {
			log.Println(err)
			return
		} else if err == io.EOF {
			log.Println("Connection closed")
			return
		}
		fmt.Printf("Received %s from address %s \n", line[:len(line)-1], c.RemoteAddr())
		writer.WriteString("Message received..")
		writer.Flush()
	}
}
