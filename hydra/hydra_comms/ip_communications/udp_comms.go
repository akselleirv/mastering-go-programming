package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
)

func main(){
	op := flag.String("type", "", "Server(s) or Client(c) ?")
	address := flag.String("addr", ":8000", "address=? host:port")
	flag.Parse()

	switch strings.ToUpper(*op) {
	case "S":
		runUDPServer(*address)
	case "C":
		runUDPClient(*address)
	}
}
// very similar to the tcp client
func runUDPClient(address string) error {
	conn, err := net.Dial("udp", address)
	if err != nil {
		return err
	}
	defer conn.Close()

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("What message would you like to send?")
	for scanner.Scan() && err == nil {
		fmt.Println("Writing ", scanner.Text())
		// \r identifier for when the message has ended
		conn.Write(scanner.Bytes())

		//Reading incoming message on the tcp conn
		buffer := make([]byte, 1024)
		_, err := conn.Read(buffer)
		if err != nil {
			log.Fatal(err)
		}
		//printing the message from the server
		fmt.Println(string(buffer))
		fmt.Println("What message would you like to send?")
	}

	return nil
}
func runUDPServer(address string) error {
	pc, err := net.ListenPacket("udp",address)
	if err != nil {
		log.Fatal(err)
	}
	defer pc.Close()
	buffer := make([]byte,1024)
	fmt.Println("Listening...")
	for {
		_,addr,_ := pc.ReadFrom(buffer)
		fmt.Printf("Received %s from address %s \n",string(buffer),addr)
		_,err := pc.WriteTo([]byte("Messaged received"),addr)
		if err != nil {
			log.Fatal("Could not write back on the connection",err)
		}
	}
}