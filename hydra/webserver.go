package main

import (
	"fmt"
	"gitlab.com/akselsle/mastering-go-programming/hydra/hlogger"
	"net/http"
)

func main(){
	logger := hlogger.GetInstance()
	logger.Println("Starting Hydra Web Service")
	http.HandleFunc("/", sroot)
	http.ListenAndServe(":8080",nil)
}

func sroot(w http.ResponseWriter,r *http.Request){
	logger := hlogger.GetInstance()
	fmt.Fprintf(w,"Welcome to the Hydra software system")

	logger.Println("Received a http request on root url")
}