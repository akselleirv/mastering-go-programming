package main

import (
	"encoding/xml"
	"fmt"
	"log"
	"os"
)

type CrewMember struct {
	ID                int      `xml:"id,omitempty"` //ignore if zero value
	Name              string   `xml:"name,attr"` //make "name" an attribute
	SecurityClearance int      `xml:"clearancelevel"`
	AccessCode        []string `xml:"accesscodes>code"` //accesscode has codes as childs
}
type ShipInfo struct {
	XMLName xml.Name `xml:"ship"`//change the struct name
	ShipID    int
	ShipClass string
	Captain   CrewMember
}

func main(){
	file, err := os.Create("xmlfile.xml")
	if err != nil {
		log.Fatal("could not create file",err)
	}
	defer file.Close()
	cm := CrewMember{Name: "Jaro",SecurityClearance: 10,AccessCode: []string{"ADA","LAL"}}
	si := ShipInfo{ShipID: 1,ShipClass: "Fighter",Captain: cm}

	b,err := xml.MarshalIndent(&si," ","	")
	if err != nil {
		log.Fatal("error",err)
	}
	fmt.Println(xml.Header,string(b))

	enc := xml.NewEncoder(file)
	enc.Indent(" ","	")
	err = enc.Encode(si)
	if err != nil {
		log.Fatal("could not encode data",err)
	}

}