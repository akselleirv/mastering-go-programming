package main

import (
	"encoding/csv"
	"log"
	"os"
)

func main(){
	records := [][]string{
		{"Jarom","5","ALA,IOI"},
		{"Hala","4","A7D,BBB"},
		{"Kay","3","CCC,CCC"},
	}

	file,err := os.Open("cfilewrite.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	w := csv.NewWriter(file)
	//w.WriteAll() instead of for-loop

	w.Comma = ';'

	for _,record := range records{
		if err := w.Write(record); err != nil{
			log.Fatal(err)
		}
	}
	w.Flush()

	err = w.Error()
	if err != nil{
		log.Fatal(err)
	}

}
