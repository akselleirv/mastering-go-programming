package main

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"log"
	"sync"
)

type crewMember struct {
	ID           int    `bson:"id"`
	Name         string `bson:"name"`
	SecClearance int    `bson:"security clearance"`
	Position     string `bson:"position"`
}
type Crew []crewMember

func main() {
	session, err := mgo.Dial("mongodb://127.0.0.1")
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	// Get collection
	personnel := session.DB("Hydra").C("Personnel")

	//Get number of documents in the collection
	n, _ := personnel.Count()
	log.Println("Number of personnel is ", n)

	//Perform simple query
	cm := crewMember{}
	personnel.Find(bson.M{"id": 2}).One(&cm)
	log.Println(cm)

	// Query with expression
	query := bson.M{
		"security clearance": bson.M{
			"$gt": 3,
		},
		"position": bson.M{
			"$in": []string{"Chief", "Engineer"},
		},
	}
	var crew Crew
	err = personnel.Find(query).All(&crew)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(crew)
	names := []struct {
		Name string `bson:"name"`
	}{}
	err = personnel.Find(query).Select(bson.M{"name": 1}).All(&names)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(names)
	/*
	//Insert
	newCr := crewMember{ID: 18, Name: "Rebecka", SecClearance: 5, Position: "Kitchen"}
	if err := personnel.Insert(newCr); err != nil {
		log.Fatal(err)
	}
	personnel.Find(bson.M{"id": 18}).One(&cm)
	log.Println(cm)

	//Update
	err = personnel.Update(bson.M{"id":18},bson.M{"$set":bson.M{"position":"Boss"}})
	if err != nil {
		log.Fatal(err)
	}
	personnel.Find(bson.M{"id": 18}).One(&cm)
	log.Println(cm)

	//Remove
	if err := personnel.Remove(bson.M{"id":18}); err != nil{
		log.Fatal(err)
	}
	personnel.Find(bson.M{"id": 18}).One(&cm)
	log.Println(cm)
	*/

	//Concurrent access
	var wg sync.WaitGroup
	count,_ := personnel.Count()
	wg.Add(count)
	for i:= 1;i<=count;i++{
		go readId(i,session.Copy(),&wg)
	}
	wg.Wait()
}
func readId(id int,sessionCopy *mgo.Session,wg *sync.WaitGroup){
	defer func() {
		sessionCopy.Close()
		wg.Done()
	}()
	p := sessionCopy.DB("Hydra").C("Personnel")
	cm := crewMember{}
	err := p.Find(bson.M{"id":id}).One(&cm)
	if err != nil{
		log.Fatalf("did not find id: %d",id)
		return
	}
	log.Println(cm)
}