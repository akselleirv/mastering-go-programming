package hlogger

import (
	"log"
	"os"
	"sync"
)

// What is the Singleton Pattern?
// * an object gets created only once then reused

// Why used the Singleton Pattern?
// * when the object need to keep state throughout our program

type hydraLogger struct {
	*log.Logger
	filename string
}

var hlogger *hydraLogger
var once sync.Once

// GetInstance create a singleton instance of the hydra logger
func GetInstance() *hydraLogger {
	//once.Do guarantee that the given function will only be called once
	once.Do(func() {
		hlogger = createLogger("hydralogger.log")
	})
	return hlogger
}

// Create a logger instance
func createLogger(fname string) *hydraLogger {
	file, _ := os.OpenFile(fname, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)

	return &hydraLogger{
		filename: fname,
		Logger:   log.New(file, "Hydra ", log.Lshortfile),
	}
}

func main() {

}
