package hydraconfigurator

import (
	"errors"
	"reflect"
)

const (
	CUSTOM uint8 = iota //enum
	JSON
	XML
)

var wrongTypeError error = errors.New("Type must be a pointer to a struct")

func GetConfiguration(confType uint8, obj interface{},filename string) (err error){
	// check if this is type pointer
	mysRValue := reflect.ValueOf(obj)
	if mysRValue.Kind() != reflect.Ptr || mysRValue.IsNil() {
		return wrongTypeError
	}
	//get and confirm the struct value
	mysRValue = mysRValue.Elem()
	// *object => object
	// reflection value of *object.Elem() => object() (Settable!!)
	// we can change the value of the underlying value (not the copy)
	if mysRValue.Kind() != reflect.Struct {
		return wrongTypeError
	}

	switch confType {
	case CUSTOM:
		err = MarshalCustomConfig(mysRValue,filename)
	case JSON:
		err = decodeJSONConfig(obj,filename)
	case XML:
		err = decodeXMLConfig(obj,filename)
	}
	return err
}
