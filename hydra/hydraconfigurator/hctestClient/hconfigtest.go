package main

import (
	"fmt"
	"gitlab.com/akselsle/mastering-go-programming/hydra/hydraconfigurator"
)

type ConfS struct {
	TS      string  `name:"testString" xml:"testString" json:"testString"`
	TB      bool    `name:"testBool" xml:"testBool" json:"testBool"`
	TF      float64 `name:"testFloat" xml:"testFloat" json:"testFloat"`
	TestInt int
}

func main() {
	configStruct := new(ConfS)
	//err := hydraconfigurator.GetConfiguration(hydraconfigurator.CUSTOM, configStruct, "configfile.conf")
	//err := hydraconfigurator.GetConfiguration(hydraconfigurator.JSON, configStruct, "configfile.json")
	err := hydraconfigurator.GetConfiguration(hydraconfigurator.XML,configStruct,"configfile.xml")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(*configStruct)

	if configStruct.TB {
		fmt.Println("bool is true")
	}
	fmt.Println(float64(4.8 * configStruct.TF))

	fmt.Println(5 * configStruct.TestInt)

	fmt.Println(configStruct.TS)
}
