package main

import (
	"encoding/xml"
	"fmt"
	"log"
)

func main() {
	type CrewMember struct {
		ID                int      `xml:"id,omitempty"` //ignore if zero value
		Name              string   `xml:"name,attr"`    //make "name" an attribute
		SecurityClearance int      `xml:"clearancelevel"`
		AccessCode        []string `xml:"accesscodes>code"` //accesscode has codes as childs
	}
	type ShipInfo struct {
		XMLName   xml.Name `xml:"ship"` //change the struct name
		ShipID    int
		ShipClass string
		Captain   CrewMember
	}
	data := []byte(` <ship>
					<ShipID>1</ShipID>
					<ShipClass>Fighter</ShipClass>
					<Captain name="Jaro">
						<clearancelevel>10</clearancelevel>
						<accesscodes>
							<code>ADA</code>
							<code>LAL</code>
						</accesscodes>
					</Captain>
				 	</ship>`)

	si := ShipInfo{}
	err := xml.Unmarshal(data, &si)
	if err != nil {
		log.Fatal("could not unmarshal xml")
	}

	fmt.Println(si.ShipID,si.ShipClass)
}
