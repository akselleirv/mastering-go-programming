package main

import (
	"encoding/xml"
	"fmt"
)

func main(){
	type CrewMember struct {
		ID                int      `xml:"id,omitempty"` //ignore if zero value
		Name              string   `xml:"name,attr"` //make "name" an attribute
		SecurityClearance int      `xml:"clearancelevel"`
		AccessCode        []string `xml:"accesscodes>code"` //accesscode has codes as childs
	}
	type ShipInfo struct {
		XMLName xml.Name `xml:"ship"`//change the struct name
		ShipID    int
		ShipClass string
		Captain   CrewMember
	}
	cm := CrewMember{ID: 1,Name: "jaro",SecurityClearance: 10,AccessCode: []string{"ADA","LAL"}}
	si := ShipInfo{ShipID: 1,ShipClass: "Fighter",Captain: cm}
	
	b,err := xml.MarshalIndent(si," ","	")
	if err != nil {
		fmt.Println("error",err)
	}
	fmt.Println(string(b))
}
