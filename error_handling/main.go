package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

var scMapping = map[string]int{
	"James": 5,
	"Kevin": 10,
	"Rahul": 9,
}

func findSC(name, server string) (int, error) {
	time.Sleep(time.Duration(rand.Intn(3)) * time.Second)

	if v,ok := scMapping[name]; !ok{
		return -1,errors.New("Crew member not found")
	}else{
		return v,nil
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	if clearance, err := findSC("Roko","Server 1"); err != nil{
		fmt.Println(err)
	}else{
		fmt.Println(clearance)
	}
	/*
	defer func() {
		if err := recover(); err != nil{
			fmt.Println("A Panic Recovered", err)
		}
	}()
	 */
}
