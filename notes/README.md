#Notes

## Best practices in Golang

Good source: https://golang.org/doc/effective_go.html

### The init function
Where to write init function to set up whatever to the required state.
Will be executed after variable declartions.

### Blank identifier
Example:
```
import _ "net/http/pprof"
```
Used to initialized other packages without direct access to their methods or functions.
Needed in order to execute the init functions of packages without using the external package directly.

### Type switch
Example:
```
var t interface{}
t = functionOfSomeType()
switch t := t.(type) {
default:
    fmt.Printf("unexpected type %T\n", t)     // %T prints whatever type t has
case bool:
    fmt.Printf("boolean %t\n", t)             // t has type bool
case int:
    fmt.Printf("integer %d\n", t)             // t has type int
case *bool:
    fmt.Printf("pointer to boolean %t\n", *t) // t has type *bool
case *int:
    fmt.Printf("pointer to integer %d\n", *t) // t has type *int
}
```
Can identify an object type and return the concrete type.
Very useful for large APIs. When we need to verify the concrete types.

Check type without switch:
```

if str, ok := value.(string); ok {
    fmt.Printf("string value is: %q\n", str)
} else {
    fmt.Printf("value is not a string\n")
}
```
### Recover

A panic can crash a program and with recover we can prevent this.
Same as try/catch in order languages.\
Example:
```
func safelyDo(work *Work) {
    defer func() {
        if err := recover(); err != nil {
            log.Println("work failed:", err)
        }
    }()
    do(work)
```
