package main

import "fmt"

func main() {
	slice := []int{1,2,3,4,5,6,7,8,9,10,15,20,21,22,23}
	find := recursionBinarySearch(slice,5,0,len(slice)-1)
	fmt.Println(find)
	find = loopBinarySearch(slice,23,0,len(slice)-1)
	fmt.Println(find)
}

func recursionBinarySearch(array []int, target, lowIndex, highIndex int) int {
	if highIndex < lowIndex {
		// value not found
		return -1
	}
	mid := int((lowIndex + highIndex) / 2) // goes to the middle of the given array
	// if the target is less than mid-value => go left
	if array[mid] > target {
		return recursionBinarySearch(array, target, lowIndex, mid)
		// if the target is larger than mid-value => go right
	} else if array[mid] < target {
		return recursionBinarySearch(array, target, mid+1, highIndex)
	} else {
		// we found our target
		return mid
	}
}

func loopBinarySearch(array []int, target, lowIndex, highIndex int) int {
	startIndex := lowIndex
	endIndex := highIndex
	var mid int
	for startIndex <= endIndex {
		mid = int((startIndex + endIndex) / 2)
		// if the target is less than mid-value => go left
		if array[mid] > target {
			endIndex = mid
			// if the target is larger than mid-value => go right
		} else if array[mid] < target {
			startIndex = mid + 1
		} else {
			// we found our target
			return mid
		}
	}
	return -1
}