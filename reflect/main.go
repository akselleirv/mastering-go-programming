package main

import (
	"fmt"
	"reflect"
)

func main() {
	type myStruct struct {
		Field1 int     `alias:"f1" desc:"field number 1"`
		Field2 string  `alias:"f2" desc:"field number 2"`
		Field3 float64 `alias:"f3" desc:"field number 3"`
	}
	mys := myStruct{2, "Hello", 2.4}
	InspectStructType(2)
	InspectStructType(&mys)

}
func InspectStructType(i interface{}) {
	mysRValue := reflect.ValueOf(i)
	if mysRValue.Kind() != reflect.Ptr {
		return
	}
	mysRValue = mysRValue.Elem()
	if mysRValue.Kind() != reflect.Struct {
		fmt.Println("this is not a struct...")
		return
	}
	mysRValue.Field(0).SetInt(15)
	mysRType := mysRValue.Type()//reflect.TypeOf(i)

	// looping for number of fields in the struct ==> 3
	for i := 0; i < mysRType.NumField(); i++ {
		fieldRType := mysRType.Field(i)
		fieldRValue := mysRValue.Field(i)
		fmt.Println(fieldRType.Name, "is of type", fieldRType.Type, "and has value", fieldRValue.Interface())
		fmt.Println("struct tags, alias: ", fieldRType.Tag.Get("alias"), "description: ", fieldRType.Tag.Get("desc"))
	}
}
