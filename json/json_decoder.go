package main

import (
	"encoding/json"
	"fmt"
)

func main(){
	type CrewMember struct {
		ID                int      `json:"id,omitempty"` //ignore if zero value
		Name              string   `json:"name"`
		SecurityClearance int      `json:"clearancelevel"`
		AccessCode        []string `json:"accesscodes"`
	}
	type ShipInfo struct {
		ShipID    int
		ShipClass string
		Captain   CrewMember
	}
	sbyte := []byte(`{"ShipID":1,"ShipClass":"Fighter","Captain":{"id":1,"name":"Jarco","clearancelevel":10,"accesscodes":["ADA","LAL"]}}`)

	si := new(ShipInfo)
	err := json.Unmarshal(sbyte,si)
	if err != nil{
		fmt.Println("error",err)
	}
	fmt.Println(si.ShipID,si.ShipClass)

	m := make(map[int]string)
	data := []byte(`{"1":"item1"}`)
	err = json.Unmarshal(data,&m)
	if err != nil{
		fmt.Println("error",err)
	}
	fmt.Println(m)
}
