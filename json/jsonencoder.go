package main

import (
	"encoding/json"
	"os"
)

// Struct field must be exportable Upper-case. "Name" and not "name".
type CrewMember struct {
	ID                int      `json:"id,omitempty"` //ignore if zero value
	Name              string   `json:"name"`
	SecurityClearance int      `json:"clearancelevel"`
	AccessCode        []string `json:"accesscodes"`
}
type ShipInfo struct {
	ShipID    int
	ShipClass string
	Captain   CrewMember
}
func main(){
	f,err := os.Create("jfile.json")
	printFatalError(err)
	defer f.Close()

	//Create data to encode
	cm := CrewMember{1, "Jarco", 10, []string{"ADA", "LAL"}}
	si := ShipInfo{1, "Fighter", cm}

	//Write to file
	err = json.NewEncoder(f).Encode(&si)
	printFatalError(err)

}
func printFatalError(err error){
	if err != nil{
		println("err",err)
	}
}