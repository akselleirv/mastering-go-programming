package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	// Struct field must be exportable Upper-case. "Name" and not "name".
	type CrewMember struct {
		ID                int      `json:"id,omitempty"` //ignore if zero value
		Name              string   `json:"name"`
		SecurityClearance int      `json:"clearancelevel"`
		AccessCode        []string `json:"accesscodes"`
	}
	type ShipInfo struct {
		ShipID    int
		ShipClass string
		Captain   CrewMember
	}

	cm := CrewMember{1, "Jarco", 10, []string{"ADA", "LAL"}}
	si := ShipInfo{1, "Fighter", cm}
	b, err := json.Marshal(si) //can take pointers
	if err != nil {
		fmt.Println("error", err)
	}
	fmt.Println(string(b))

	//Marshal maps
	m := map[string]int{"Item1": 1, "Item2": 2}
	c, err := json.Marshal(m)
	if err != nil {
		fmt.Println("error", err)
	}
	fmt.Println(string(c))

	//Marshal slices
	s := []CrewMember{cm,{2,"Jim",5,[]string{"TLT","RAR"}}}
	c, err = json.Marshal(s)
	if err != nil {
		fmt.Println("error", err)
	}
	fmt.Println(string(c))
}
