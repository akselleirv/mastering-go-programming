package main

import (
	"fmt"
	"gitlab.com/akselsle/mastering_go_programming/factory_design_pattern/appliances"
)

// What is Factory Pattern?
// * a creational pattern
// * caller does not create objects directly
// * the factory function or object handles the creation of new
// objects based on some conditions

// Why use the pattern?
// * makes your code easily expandable
// * separation of concerns, only troubleshoot in one place
// * efficient team work on large projects
// * and many more reasons..

// How to implement in Go?
// * Create an interface. That will act as a father for all the other types.
// * Create concrete types. The types that hide behind the factory.
// * Create factory function or method.
func main(){
	fmt.Println("Enter preferred appliance type")
	fmt.Println("0: Stove")
	fmt.Println("1: Fridge")
	fmt.Println("2: Microwave")

	var myType int
	fmt.Scan(&myType)

	myAppliance, err := appliances.CreateAppliance(myType)
	if err != nil{
		fmt.Println(err)
	}else{
		myAppliance.Start()
		fmt.Println(myAppliance.GetPurpose())
	}
}
