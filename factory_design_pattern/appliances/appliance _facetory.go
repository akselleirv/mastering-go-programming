package appliances

import "errors"

// Main interface to describe the appliances
type Appliance interface {
	Start()
	GetPurpose() string
}

// Our appliance types (enum)
const (
	STOVE = iota
	FRIDGE
	MICROWAVE
)

func CreateAppliance(myType int) (Appliance, error) {
	switch myType {
	case STOVE:
		return new(Stove), nil
	case FRIDGE:
		return new(Fridge), nil
	case MICROWAVE:
		return new(MicroWave),nil
	default:
		return nil, errors.New("invalid appliance type")
	}
}
