package appliances

type MicroWave struct {
	typeName string
}

func (mw * MicroWave) Start(){
	mw.typeName = " MicroWave "
}

func (mw *MicroWave) GetPurpose() string{
	return "I am a " + mw.typeName +  " I heat stuff up!!"
}