package main

import "fmt"

type magicStore struct {
	value interface{}
	name  string
}

func (ms *magicStore) SetValue(v interface{}) {
	ms.value = v
}
func (ms *magicStore) GetValue() interface{} {
	return ms.value
}

func NewMagicStore(nm string) *magicStore {
	return &magicStore{name: nm}
}

func main() {
	// Testing of func printType
	printType("text")
	printType(10)
	printType(20.20)

	//Testing of magicStore
	istore := NewMagicStore("Integer Store")
	istore.SetValue(10)
	if v, ok := istore.GetValue().(int); ok {
		v *= 10
		fmt.Println(v)
	}
	sstore := NewMagicStore("String Store")
	sstore.SetValue("Hello")
	if v,ok := sstore.GetValue().(string); ok{
		v += " World"
		fmt.Println(v)
	}

}

func printType(i interface{}) {
	switch i := i.(type) {
	case string:
		fmt.Println("This is a string", i)
	case int:
		fmt.Println("This is an int", i)
	case float64:
		fmt.Println("This is a float", i)
	}
}
